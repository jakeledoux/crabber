beautifulsoup4==4.9.3
blis==0.7.4
cached-property==1.5.2
cairocffi==1.2.0
catalogue==1.0.0
certifi==2020.12.5
cffi==1.14.4
chardet==4.0.0
click==7.1.2
crabatar==1.0.3
cymem==2.0.5
flake8==3.8.4
Flask==1.1.2
Flask-Limiter==1.4
Flask-SQLAlchemy==2.4.4
gizeh==0.1.11
idna==2.10
itsdangerous==1.1.0
Jinja2==2.11.3
limits==1.5.1
Mako==1.1.4
MarkupSafe==1.1.1
mccabe==0.6.1
more-itertools==8.6.0
murmurhash==1.0.5
numpy==1.20.0
ordered-set==3.1.1
ordered-set-stubs==0.1.3
passlib==1.7.4
Pillow==8.1.0
plac==1.1.3
poetry-version==0.1.5
preshed==3.0.5
pycodestyle==2.6.0
pycparser==2.20
pydantic==1.7.3
pyflakes==2.2.0
python-dateutil==2.8.1
python-editor==1.0.4
redis==3.5.3
requests==2.25.1
ruamel.yaml==0.15.100
six==1.15.0
soupsieve==2.1
spacy==2.3.5
SQLAlchemy==1.3.22
srsly==1.0.5
thinc==7.4.5
toml==0.10.2
tomlkit==0.5.11
tqdm==4.56.0
urllib3==1.26.3
wasabi==0.8.2
Werkzeug==1.0.1
